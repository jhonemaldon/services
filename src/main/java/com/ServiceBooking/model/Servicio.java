package com.ServiceBooking.model;


import com.ServiceBooking.controller.dto.ServiceDto;
import com.ServiceBooking.repository.RoleEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Document
public class Servicio {

    @Id
    private String oid; // String
    private String nameService;

    @Indexed( unique = true )
    String email;
    String passwordHash;
    List<RoleEnum> roles;
    Date createdAt;

    ///////// CONSTRUCTOR //////////
//    public Servicio(String oid, String nameService) {
//        this.oid = oid;
//        this.nameService = nameService;
//    }

    public Servicio() {
    }

    public Servicio(ServiceDto serviceDto) {
        nameService = serviceDto.getNameService();
        email = serviceDto.getEmail();
        createdAt = new Date();
        roles = new ArrayList<>( Collections.singleton( RoleEnum.USER ) );
        //TODO uncomment this line
        passwordHash = BCrypt.hashpw( serviceDto.getPassword(), BCrypt.gensalt() );
    }


    /////////// GETTERS ////////////
    public String getId()
    {
        return oid;
    }
    public String getOid() {
        return oid;
    }
    public String getNameService() {
        return nameService;
    }
    public String getEmail()
    {
        return email;
    }
    public Date getCreatedAt()
    {
        return createdAt;
    }
    public String getPasswordHash()
    {
        return passwordHash;
    }
    public List<RoleEnum> getRoles()
    {
        return roles;
    }

    ///////// SETTER ///////

    public void setOid(String oid) {
        this.oid = oid;
    }
    public void setNameService(String nameService) {
        this.nameService = nameService;
    }

    public void update( ServiceDto serviceDto )
    {
        //nameService = serviceDto.getNameService();
        this.nameService = serviceDto.getNameService();
        this.email = serviceDto.getEmail();
        //TODO uncomment these lines
        if ( serviceDto.getPassword() != null )
        {
            this.passwordHash = BCrypt.hashpw( serviceDto.getPassword(), BCrypt.gensalt() );
        }
    }
}