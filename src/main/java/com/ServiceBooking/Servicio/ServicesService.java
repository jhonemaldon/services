package com.ServiceBooking.Servicio;

import com.ServiceBooking.controller.dto.ServiceDto;
import com.ServiceBooking.error.exception.UserNotFoundException;
import com.ServiceBooking.model.Servicio;

import java.util.Date;
import java.util.List;

public interface ServicesService {

    Servicio create(ServiceDto serviceDto);

    Servicio findById(String oid)
        throws UserNotFoundException;

    Servicio findByEmail(String email)
        throws UserNotFoundException;

    //Servicio updateName(String name);

    Servicio updateService(ServiceDto serviceDto, String oid);

    //List<Servicio> findAllService(Servicio servicio);
    List<Servicio> findAllService();

    boolean deleteService(String oid);

}