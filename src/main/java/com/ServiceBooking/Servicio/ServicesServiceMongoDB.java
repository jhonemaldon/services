package com.ServiceBooking.Servicio;

import com.ServiceBooking.controller.dto.ServiceDto;
import com.ServiceBooking.error.exception.UserNotFoundException;
import com.ServiceBooking.model.Servicio;
import com.ServiceBooking.repository.ServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicesServiceMongoDB implements ServicesService{

    private final ServicesRepository servicesRepository;

    public ServicesServiceMongoDB (@Autowired ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
    }

    @Override
    public Servicio create(ServiceDto serviceDto){
        return servicesRepository.save(new Servicio(serviceDto));
    }

//    @Override
//    public Servicio findById(String oid){
//        return servicesRepository.findServicioByOid(oid);
//    }

    @Override
    public Servicio findById(String oid){
        Optional<Servicio> optionalServicio = servicesRepository.findById( oid );
        if ( optionalServicio.isPresent() )
        {
            return optionalServicio.get();
        }
        else
        {
            throw new UserNotFoundException();
        }
    }

    @Override
    public Servicio findByEmail( String email )
            throws UserNotFoundException
    {
        Optional<Servicio> optionalUser = servicesRepository.findByEmail( email );
        if ( optionalUser.isPresent() )
        {
            return optionalUser.get();
        }
        else
        {
            throw new UserNotFoundException();
        }
    }

    @Override
    public Servicio updateService(ServiceDto serviceDto, String oid) {
        if (servicesRepository.findById(oid).isPresent()){
            Servicio servicio = servicesRepository.findById(oid).get();
            servicio.update(serviceDto);
            servicesRepository.save(servicio);
            return servicio;
        }
        return null;
    }

//    @Override
//    public List<Servicio> findAllService(Servicio servicio) {
//        return servicesRepository.findAll();
//    }

    @Override
    public List<Servicio> findAllService(){
        return servicesRepository.findAll();
    }

    @Override
    public boolean deleteService(String oid){
        if (servicesRepository.existsById( oid )) {
            servicesRepository.deleteById( oid );
            return true;
        }
        return false;
    }

    /*@Override
    public List<Servicio> all() {
        return new List<Servicio>(ServicesService.values());
    }*/
}
