package com.ServiceBooking.controller.health;

import com.ServiceBooking.Servicio.ServicesService;
import com.ServiceBooking.controller.dto.ServiceDto;
import com.ServiceBooking.model.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( "v1/health" )
public class HealthController
{

//    @Autowired
//    ServicesService servicesService;

    @GetMapping
    public String all()
    {
        //createNewUser();
        return "API Working OK!";
    }

//    private void createNewUser() {
//        ServiceDto serviceDto = new ServiceDto("Ingeniería", "isaac@email.com", "123");
//        Servicio servicio = servicesService.create( serviceDto );
//        System.out.println("Quemando usuario de prueba: " + servicio.getId());
//    }

}