package com.ServiceBooking.controller.auth;

import com.ServiceBooking.Servicio.ServicesService;
import com.ServiceBooking.error.exception.InvalidCredentialsException;
import com.ServiceBooking.model.Servicio;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;

import static com.ServiceBooking.config.Constants.CLAIMS_ROLES_KEY;
import static com.ServiceBooking.config.Constants.TOKEN_DURATION_MINUTES;

@RestController
@RequestMapping( "v1/auth" )
public class AuthController
{

    @Value( "${app.secret}" )
    String secret;

    private final ServicesService servicesService;

    public AuthController( @Autowired ServicesService servicesService )
    {
        this.servicesService = servicesService;
    }

    @PostMapping
    public TokenDto login( @RequestBody LoginDto loginDto )
    {
        Servicio servicio = servicesService.findByEmail( loginDto.email );
        if ( BCrypt.checkpw( loginDto.password, servicio.getPasswordHash() ) )
        {
            return generateTokenDto( servicio );
        }
        else
        {
            throw new InvalidCredentialsException();
        }

    }

    private String generateToken( Servicio servicio, Date expirationDate )
    {
        return Jwts.builder()
                .setSubject( servicio.getId() )
                .claim( CLAIMS_ROLES_KEY, servicio.getRoles() )
                .setIssuedAt(new Date() )
                .setExpiration( expirationDate )
                .signWith( SignatureAlgorithm.HS256, secret )
                .compact();
    }

    private TokenDto generateTokenDto( Servicio servicio )
    {
        Calendar expirationDate = Calendar.getInstance();
        expirationDate.add( Calendar.MINUTE, TOKEN_DURATION_MINUTES );
        String token = generateToken( servicio, expirationDate.getTime() );
        return new TokenDto( token, expirationDate.getTime() );
    }
}