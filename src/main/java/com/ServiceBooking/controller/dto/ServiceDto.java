package com.ServiceBooking.controller.dto;

public class ServiceDto {
    //String oid;
    String nameService;
    String email;
    String password;

    ///////// CONSTRUCTOR //////////
    public ServiceDto() {
    }

    public ServiceDto( String nameService, String email, String password) {
        this.nameService = nameService;
        this.email = email;
        this.password = password;
    }

    /////////// GETTERS ////////////

    public String getNameService() {
        return nameService;
    }

    public String getEmail()
    {
        return email;
    }

//    public String getOid() {
//        return oid;
//    }
    public String getPassword()
    {
        return password;
    }
}
