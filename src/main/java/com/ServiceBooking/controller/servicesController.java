package com.ServiceBooking.controller;

import com.ServiceBooking.Servicio.ServicesService;
import com.ServiceBooking.controller.dto.ServiceDto;
import com.ServiceBooking.model.Servicio;
import com.ServiceBooking.repository.ServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/services")
public class servicesController {

    private final ServicesService servicesService;

    public servicesController(@Autowired ServicesService servicesService) {
        this.servicesService = servicesService;
    }

    @PostMapping
    public Servicio create(@RequestBody ServiceDto serviceDto) {
        return servicesService.create(serviceDto);
    }

    @GetMapping("/{oid}")
    public Servicio findById(@PathVariable String oid) {
        return servicesService.findById(oid);
    }

    @GetMapping("/all")
    public List<Servicio> getServiceList() {
        return servicesService.findAllService();
    }

    @PutMapping("/{oid}")
    public Servicio updateService(@RequestBody ServiceDto serviceDto, @PathVariable String oid) {
        return servicesService.updateService(serviceDto, oid);
    }

    /*@PutMapping("/{oid}")
    public ResponseEntity<Servicio> updateTutorial(@PathVariable("oid") String oid, @RequestBody Servicio servicio) {
        Optional<Servicio> serviceOptional = Optional.ofNullable(servicesService.findById(oid));
        if (serviceOptional.isPresent()) {
            //servicio = servicio.getName();
            servicio.setName(servicio.getName());
            return new ResponseEntity<>(servicesService.create(servicio), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }*/


    @DeleteMapping("/{oid}")
    @RolesAllowed("ADMIN")
    boolean deleteService(@PathVariable String oid) {
        return servicesService.deleteService(oid);
    }
}