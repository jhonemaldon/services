package com.ServiceBooking.repository;

import com.ServiceBooking.model.Servicio;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ServicesRepository extends MongoRepository<Servicio, String> {

    //Servicio save(Servicio serviceDto);

    //Servicio findServicioByOid(String oid);

    Optional<Servicio> findByEmail(String email );

    //List<Servicio> findAll();

    boolean deleteServicioByOid(String oid);

    ///Servicio insert(String oid, String name);

    ///Servicio save(String oid, String name);
}